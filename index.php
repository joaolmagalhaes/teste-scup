<?php 
	include('header.php'); 
	include('function.php'); 

	$ddd_lista = get_json_to_array('http://private-fe2a-scuptel.apiary-mock.com/ddd/details');
	$tarifa = get_json_to_array('http://private-fe2a-scuptel.apiary-mock.com/ddd/pricing');
	$planos = get_json_to_array('http://private-fe2a-scuptel.apiary-mock.com/plans');
	
?>
	<body>
		<div class="container">
			<div class="corpo">
				<div class="logo">
					<img src="assets/images/scup-logo.png" alt="" class="scup-logo">
					<h1 class="scup-fale">Fale +</h1>
				</div>
				<form action="POST" class="comparador">
					<div class="row">
						<div class="col-md-12 titulo">
							<h2 class="texto-titulo">Comparar Planos</h2>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<label for="origem">Origem</label>
							<input type="text" name="origem" id="origem" placeholder="DDD"><i class="material-icons" data-toggle="modal" data-target="#myModal" data-localizacao="origem" disabled>add_box</i>
						</div>
						<div class="col-md-4">
							<label for="destino">Destino</label>
							<input type="text" name="destino" id="destino" placeholder="DDD"><i class="material-icons" data-toggle="modal" data-target="#myModal" data-localizacao="destino" disabled>add_box</i>
						</div>
						<div class="col-md-4">
							<label for="tempo">Tempo</label>
							<input type="text" name="tempo" id="tempo" placeholder="MM">
						</div>
					</div>
				</form>
				<div class="resultado">
					<div class="row">	
						<?php foreach ($planos as $key => $plano) { ?>
							<div class="col-md-3 planos">
								<span class="plano"><?php echo $plano['plan']; ?></span>
								<span class="valor"><span class="valor-inteiro-<?php echo $plano['time']; ?>"> - </span></span>
							</div>
						<?php } ?>
						<div class="col-md-3 planos">
							<span class="plano">Normal</span>
							<span class="valor"><span class="valor-inteiro-normal"> - </span></span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Modal -->
		<div class="modal fade" id="myModal" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">	
					<div class="modal-body">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<div class="tabela-ddd">
							<?php foreach ($ddd_lista as $key => $ddd_local) { ?>
								<div class="linha-tabela">
								<a href="#" class="ddd-link" data-value="<?php echo $ddd_local['ddd']; ?>">
									<span class="ddd"><?php echo $ddd_local['ddd']; ?> - </span>
									<span class="localidade"><?php echo $ddd_local['city']; ?></span>
								</a>
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
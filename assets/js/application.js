'use strict';

var getJson = function (url) {
    var result = "";
    jQuery.ajax({
        url: url,
        type: 'GET',
        async: false,
        dataType: 'JSON',
        success: function (dataReturn) {
        	
          	result = dataReturn.data;
        }
    });
    return result;
};

$(document).ready(function(){

	$('.material-icons').click(function(){
		var location = $(this).attr('data-localizacao');
		
		$('.ddd-link').click(function(){
			if('origem' == location){
				$('#origem').val($(this).attr('data-value'));
				location = '';
				$('#myModal').modal('toggle');
			} else if ('destino' == location){
				$('#destino').val($(this).attr('data-value'));
				location = '';
				$('#myModal').modal('toggle');
			}
			
		});

	});

    var tarifas = getJson('http://private-fe2a-scuptel.apiary-mock.com/ddd/pricing');
    var planos = getJson('http://private-fe2a-scuptel.apiary-mock.com/plans');

    $('#tempo').keyup(function(){
    	if('' != $('#origem').val() && '' != $('#destino').val() ) {
    		jQuery.each(tarifas, function(key, tarifa){
    			if($('#origem').val() == tarifa.origin && $('#destino').val() == tarifa.destiny) {
    				jQuery.each(planos, function(key, plano){
    					var tempo = $('#tempo').val();
    					var franquia = plano.time;
    					var valor = tarifa.price;

    					var minutos = (tempo - franquia);
    					var resultadoPlanos = "";
    					var resultadoNormal = "";

    					if(0 < minutos) {
    						resultadoPlanos = parseFloat(Math.round((minutos *(valor * 1.1)) * 100) / 100).toFixed(2);
    						resultadoNormal = parseFloat(Math.round((minutos * valor) * 100) / 100).toFixed(2);
    					} else {
    						resultadoPlanos = 0.00;
    						resultadoNormal = 0.00;
    					}

    					$('.valor-inteiro-'+plano.time).text('R$ '+resultadoPlanos);
    					$('.valor-inteiro-normal').text('R$ '+resultadoNormal);
    					
    				});
    			}
    			
    		});
    	}
    });
});
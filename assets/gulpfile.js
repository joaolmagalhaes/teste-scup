var gulp = require('gulp');
var sass = require('gulp-sass');
var minify = require('gulp-minify');
var rename = require('gulp-rename');

gulp.task('sass', function() {
  return gulp.src('scss/**/*.scss') // Gets all files ending with .scss in app/scss and children dirs
    .pipe(sass({outputStyle: 'compressed'}))
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('css'))
});

gulp.task('compress', function (cb) {
    gulp.src('js/*.js')
    .pipe(minify({
    	ext:{
    		src:'.js',
    		min:'.min.js'
    	},
    	exclude: ['tasks'],
    	ignoreFiles: ['.min.js']
    }))
    .pipe(gulp.dest('js'))
});

gulp.task('watch', function(){
	gulp.watch('scss/**/*.scss', ['sass']);
	gulp.watch('js/*.js', ['compress']);
});	
<?php 
	function get_json_to_array($json_url) {
		$json = file_get_contents($json_url);

		$obj = json_decode($json);

		$data = $obj->data;
		$data_array = array();

		foreach ($data as $key => $value) {
			
			$data_array[$key] = get_object_vars($value);
		}

		return $data_array;
	}